public class Circle {
    private int radius;
    Point center;
    public Circle(int radius,int centerX,int centerY){
        this.radius = radius;
        center = new Point(centerX,centerY);
    }

    public int area(){
        return (int) (Math.PI*radius*radius);
    }

    public int perimeter(){
        return (int) (2*Math.PI*radius);
    }

    public boolean intersect(Circle circle){
        int x =Math.abs( center.xCord - circle.center.xCord);
        int y =Math.abs(center.yCord - circle.center.yCord);
        int hipotenus = (int) Math.sqrt(Math.abs((Math.pow(x,2) - Math.pow(y,2))));
        return hipotenus <= radius + circle.radius;
    }
}
