import java.util.ArrayList;

public class Rectangle {
    private int sideA;
    private int sideB;
    private Point topLeft;
    public Rectangle(int x,int y,int sideA, int sideB){
        topLeft = new Point(x,y);
        this.sideA= sideA;
        this.sideB = sideB;
    }
    public int area(){
        return sideA*sideB;
    }

    public int perimater(){
        return (sideA+sideB)*2;
    }

    public int[][] corners(){
        return new int[][]{{topLeft.xCord,topLeft.yCord},{topLeft.xCord + sideA,topLeft.yCord},{topLeft.xCord + sideA,topLeft.yCord + sideB},{topLeft.xCord,topLeft.yCord + sideB}};
    }




}
