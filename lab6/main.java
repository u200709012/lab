import java.awt.Point;
import java.util.Arrays;

public class main{

    public static void main(String[] args){
        Point point = new Point(0,0);
        Rectangle rectangle = new Rectangle(0,0,10,20);
        Circle circle1 = new Circle(10,0,0);
        Circle circle2 = new Circle(1,0,20);
        System.out.println(circle1.intersect(circle2));
    }
}