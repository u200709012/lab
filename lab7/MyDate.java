public class MyDate {
    int day;
    int month; // should be incremented by one before printing
    int year;

    public MyDate(int day, int month,int year ) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public String toString(){
        return year + "-"+(month + 1 < 10 ? "0" : "") + (month) + "-" +(day < 10 ? "0" : "") + day;
    }


    int[] maxDays = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    public void dateHelper(int y, int m, int d, boolean c) {
        //Day handler
        while (d > 31) {
            m++;
            d -= 31;
        }
        if (d < 1) {
            m--;
            d = maxDays[m - 1] + d;
        }
        //Month hendler
        while (m > 12) {
            m -= 12;
            y++;
        }
        while (m < 1) {
            y--;
            m += 12;
        }
        //28 29
        if (y % 4 == 0) {
            maxDays[1] = 29;
        } else {
            maxDays[1] = 28;
        }
        //correct day
        if (c) {
            if (d > maxDays[m - 1]) {
                d = d - maxDays[m - 1];
                m++;
            }
        } else {
            if (d > maxDays[m - 1]) {
                d = maxDays[m - 1];
            }
        }
        year = y;
        month = m;
        day = d;
    }
    public void incrementDay() {
        dateHelper(year, month, day + 1, true);
    }
    public void incrementYear(int i) {
        dateHelper(year + i, month, day, true);
    }
    public void decrementDay() {
        dateHelper(year, month, day - 1, true);
    }
    public void decrementYear() {
        dateHelper(year - 1, month, day, false);
    }
    public void decrementMonth() {
        dateHelper(year, month - 1, day, true);
    }
    public void incrementDay(int i) {
        dateHelper(year, month, day + i, true);
    }
    public void decrementMonth(int i) {
        dateHelper(year, month - i, day, false);
    }
    public void decrementDay(int i){
        dateHelper(year, month, day - i,true );
    }
    public void incrementMonth(int i){
        dateHelper(year, month + i, day,false);
    }
    public void decrementYear(int i){
        dateHelper(year - i, month, day,true);
    }
    public void incrementMonth(){
        dateHelper(year, month + 1, day,true);
    }
    public void incrementYear(){
        dateHelper(year + 1, month, day,true);
    }

    public boolean isBefore(MyDate d) {
        int a = Integer.parseInt(toString().replaceAll("-", "")) ;
        int b = Integer.parseInt(d.toString().replaceAll("-", ""));
        return a < b;
    }

    public boolean isAfter(MyDate d) {
        int a = Integer.parseInt(toString().replaceAll("-", "")) ;
        int b = Integer.parseInt(d.toString().replaceAll("-", ""));
        return a > b;
    }


    public int dayDifference(MyDate d) {
        int diff = 0;
        if (isBefore(d)){
            MyDate date = new MyDate(day,month,year);
            while (date.isBefore(d)){
                date.incrementDay();
                diff++;
            }
        }else if(isAfter(d)){
            MyDate date = new MyDate(day,month,year);
            while (date.isAfter(d)){
                date.decrementDay();
                diff++;
            }

        }
        return diff;
    }


}
