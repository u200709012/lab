public class MyTime {
    int hour;
    int minute;

    public MyTime(int hour, int minute){
        this.hour = hour;
        this.minute = minute;
    }

    public void incrementHour(){
        if (hour < 23){
            hour++;
        }else {
            hour = 0;
        }
    }

    public void decrementHour(){
        if (hour > 0){
            hour--;
        }else {
            hour = 23;
        }
    }

    public void incrementMinute(){
        if (minute < 59){
            minute++;
        }else {
            minute = 0;
        }
    }

    public void decrementMinute(){
        if (minute > 0){
            minute--;
        }else {
            minute = 59;
        }
    }



}
