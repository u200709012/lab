public class MyDateTime {
    MyTime time;
    MyDate date;

    public MyDateTime(MyDate date,MyTime time){
        this.date = date;
        this.time = time;
    }

    public String toString(){
        return date.year + "-"+(date.month + 1 < 10 ? "0" : "") + (date.month) + "-" +(date.day < 10 ? "0" : "") + date.day + " "  + (time.hour  < 10 ? "0" : "") + (time.hour) + ":" +(time.minute < 10 ? "0" : "") + time.minute;
    }


    public void incrementDay(){
        date.incrementDay();
    }

    public void incrementHour(){
        time.incrementHour();
        if (time.hour == 0) {
            date.incrementDay();
        }
    }

    public void incrementHour(int i){
        for (int j = 0; j < i; j++) {
            time.incrementHour();
            if (time.hour == 0) {
                date.incrementDay();
            }
        }
    }

    public void decrementHour(int i){
        for (int j = i; j > 0; j--) {
            time.decrementHour();
            if (time.hour == 23) {
                date.decrementDay();
            }
        }
    }

    public void incrementMinute(int i){
        for (int j = 0; j < i; j++) {
            time.incrementMinute();
            dayIncrementChecker();
            if (time.minute == 0){
                time.incrementHour();
            }
        }

    }

    public void decrementMinute(int i){
        for (int j = i; j > 0; j--) {
            if (time.minute == 0){
                time.decrementHour();
            }
            time.decrementMinute();
            dayDecrementChecker();
        }

    }

    public void incrementYear(int i){
        date.incrementYear(i);
    }

    public void decrementDay(){
        date.decrementDay();
    }

    public void decrementYear(){
        date.decrementYear();
    }

    public void decrementMonth(){
        date.decrementMonth();
    }

    public void incrementDay(int i){
        date.incrementDay(i);
    }

    public void decrementMonth(int i){
        date.decrementMonth(i);
    }

    public void decrementDay(int i){
        date.decrementDay(i);
    }

    public void incrementMonth(int i){
        date.incrementMonth(i);
    }

    public void decrementYear(int i){
        date.decrementYear(i);
    }

    public void incrementMonth(){
        date.incrementMonth();
    }

    public void incrementYear(){
        date.incrementYear();
    }

    public void dayIncrementChecker(){
        if (time.minute == 0 & time.hour == 0){
            date.incrementDay();
        }
    }

    public void dayDecrementChecker(){
        if (time.minute == 0 & time.hour == 0){
            date.decrementDay();
        }
    }


}
