import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class giveHint {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        Random rand = new Random(); //Creates an object from Random class
        int number =rand.nextInt(100); //generates a number between 0 and 99


        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");
        int guess = reader.nextInt(); //Read the user input
        while (guess != -1){
            if (guess == number){
                System.out.println("Cong");
                break;
            }else{
                System.out.println("-1 to quit or guess");
            }
            if (guess < number){
                System.out.println("You have to guess biger number");
            }else {
                System.out.println("You have to guess smaller number");
            }
            guess = reader.nextInt(); //Read the user input
        }


        reader.close(); //Close the resource before exiting
    }


}