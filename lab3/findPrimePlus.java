import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class findPrimePlus {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        System.out.print("Number: ");
        int num = reader.nextInt(); //Read the user input
        boolean[] primeBoard = new boolean[num ];
        ArrayList<Integer> arr = new ArrayList<>();
        arr.add(2);
        for (int i = 3; i < num; i+=2) {
            if (primeBoard[i - 1] == false){
                arr.add(i);
                int temp = i;
                int multiply = 1;
                while (temp*multiply < num){
                    primeBoard[temp*multiply  - 1] = true;
                    multiply+=1;
                }
            }
        }
        System.out.println(arr);
        reader.close(); //Close the resource before exiting
    }


}