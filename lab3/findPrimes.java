import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class findPrimes {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        System.out.print("Number: ");
        int num = reader.nextInt(); //Read the user input
        ArrayList<Integer> res = new ArrayList<>();
        res.add(2);
        outerloop:
        for (int i = 3; i < num; i+=2) {
            for (int j = 3; j <= Math.sqrt(i) ; j++) {
                if (i % j == 0){
                    continue outerloop;
                }
            }
            res.add(i);

        }
        System.out.println(res);
        reader.close(); //Close the resource before exiting
    }


}