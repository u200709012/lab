import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

	public static void main(String[] args) throws IOException {
		Scanner reader = new Scanner(System.in);
		char[][] board = { { ' ', ' ', ' ' }, { ' ', ' ', ' ' }, { ' ', ' ', ' ' } };
		boolean full = false;
		int moves = 0;

		while (!full) {
			boolean validMove1 = false;
			boolean validMove2 = false;

			//player 1 move
			while (!validMove1){

			printBoard(board);
			System.out.print("Player 1 enter row number:");
			int row = reader.nextInt();

			System.out.print("Player 1 enter column number:");
			int col = reader.nextInt();

			if ( row > 3 || col > 3){
				System.out.println("not valid move");
			}
			else if (board[row - 1][col - 1] == ' '){
				board[row - 1][col - 1] = 'X';
				validMove1 = true;
				moves+=1;
			}else {
				System.out.println("already occupied");
			}
		}
			//Full check
			if (moves == 9){
				full = true;
			}

			//Player 2 move
			while (!validMove2){

				printBoard(board);
				System.out.print("Player 2 enter row number:");
				int row = reader.nextInt();

				System.out.print("Player 2 enter column number:");
				int col = reader.nextInt();

				if (board[row - 1][col - 1] == ' '){
					board[row - 1][col - 1] = '0';
					validMove2 = true;
					moves+=1;
				}else {
					System.out.println("already occupied");
				}
				printBoard(board);
			}
		}
		reader.close();
	}

	public static void printBoard(char[][] board) {
		System.out.println("    1   2   3");
		System.out.println("   -----------");
		for (int row = 0; row < 3; ++row) {
			System.out.print(row + 1 + " ");
			for (int col = 0; col < 3; ++col) {
				System.out.print("|");
				System.out.print(" " + board[row][col] + " ");
				if (col == 2)
					System.out.print("|");

			}
			System.out.println();
			System.out.println("   -----------");

		}

	}

}