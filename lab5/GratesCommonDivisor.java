public class GratesCommonDivisor{
    public static void main(String[] args){

        System.out.println(gratesCommon(100,45));
    }

    public static int gratesCommon(int num1, int num2){
        int divisor = num1 / num2;
        int remainder = num1 - num2 * divisor;
        if (remainder < 1){
            return num2;
        }else if(remainder == 1){
            return -1;
        }
        return gratesCommon(num2,remainder);
    }
}