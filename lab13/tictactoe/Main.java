package tictactoe;

import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InvalidMoveException {
        Scanner reader = new Scanner(System.in);

        Board board = new Board();

        System.out.println(board);
        while (!board.isEnded()) {

            int player = board.getCurrentPlayer();

            int row = 0;
            System.out.print("Player " + player + " enter row number:");
            try {
                row = Integer.valueOf(reader.nextLine());
            }catch (NumberFormatException numberFormatException){
                System.out.println("Input is not a valid value");
                throw numberFormatException;
            }

            System.out.print("Player " + player + " enter row number:");
            int col = 0;
            try {
                col = Integer.valueOf(reader.nextLine());
            }catch (NumberFormatException numberFormatException){
                System.out.println("Input is not a valid value");
                throw numberFormatException;
            }
            System.out.println(board.isOccupied(row,col));
            if(!(0 < row & row < 4) || !(0 < col & col < 4) || board.isOccupied(row,col)){
                throw new InvalidMoveException();
            }

            board.move(row, col);
            System.out.println(board);
        }


        reader.close();
    }


}
