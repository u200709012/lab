package tictactoe;

public class InvalidMoveException extends Exception{
    public InvalidMoveException(){
        super("Input should be between 1 - 3 and was't taken");
    }
}
